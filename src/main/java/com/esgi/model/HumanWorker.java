package com.esgi.model;

import com.esgi.exception.EvilCorpException;
import com.esgi.utils.Log;

/**
 * Class of a human worker
 */
public class HumanWorker extends Worker {

    /**
     * The constructor for a human worker
     * @param name the name of the human worker
     */
    public HumanWorker(String name) {
        super(name);
    }

    /**
     * Set a human worker to eat for a specified duration
     * @param startHour the beginning time of the work
     * @param endHour the ending time of the work
     */
    public void eat(int startHour, int endHour) throws EvilCorpException {
        if(currentAffectation != null) {
            Log.register(currentAffectation, "Eat", startHour, endHour);
        }
    }

    /**
     * Set a worker to sleep for a specified duration
     * @param startHour the beginning time of the work
     * @param endHour the ending time of the work
     */
    public void sleep(int startHour, int endHour) throws EvilCorpException {
        if(currentAffectation != null) {
            Log.register(currentAffectation, "Sleep", startHour, endHour);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Worker)) return false;

        HumanWorker worker = (HumanWorker) o;

        return name.equals(worker.name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
