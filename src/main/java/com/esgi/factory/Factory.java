package com.esgi.factory;

import com.esgi.model.Affectation;
import com.esgi.model.Tasks;
import com.esgi.model.Worker;

import java.util.ArrayList;
import java.util.List;

/**
 * Class of factory
 * The factory manages the affections of workers to a task and multiple activities
 */
public class Factory {

    /**
     * The list of registered affectations in the factory
     */
    private List<Affectation> registeredAffectation;

    /**
     * The constructor of the factory
     */
    public Factory() {

        this.registeredAffectation = new ArrayList<Affectation>();
    }

    public List<Affectation> getRegisteredAffectation() {
        return registeredAffectation;
    }

    /**
     * Create an affectation of a worker to a task, with a starting date
     * @param worker the worker to affect to
     * @param date the beginning date of the affectation
     * @param task the task the worker must be affect to
     */
    public void createAffectation(Worker worker, String date, Tasks task) {

        Affectation newAffectation = new Affectation(worker, date, task);

        registeredAffectation.add(newAffectation);
        worker.updateAffectation(newAffectation);
    }
}
