package com.esgi.model;

import com.esgi.exception.EvilCorpException;
import com.esgi.utils.Log;

/**
 * Class of a worker
 */
public class Worker {

    /**
     * The name of the worker
     */
    protected String name;

    /**
     * The current affectation to a task of the worker
     */
    protected Affectation currentAffectation;

    /**
     * The constructor for the worker
     * @param name the name of the worker
     */
    public Worker(String name) {
        this.name = name;
        this.currentAffectation = null;
    }

    public String getName() {
        return name;
    }

    public void updateAffectation(Affectation affectation) {
        this.currentAffectation = affectation;
    }

    /**
     * Set a worker to work for a specified duration
     * @param startHour the beginning time of the work
     * @param endHour the ending time of the work
     */
    public void work(int startHour, int endHour) throws EvilCorpException {
        if(currentAffectation != null) {
            Log.register(currentAffectation, "Work", startHour, endHour);
        }
    }

    /**
     * Set a worker to another activity for a specified duration
     * @param startHour the beginning time of the activity
     * @param endHour the ending time of the activity
     */
    public void other(int startHour, int endHour) throws EvilCorpException {
        if(currentAffectation != null) {
            Log.register(currentAffectation, "Other", startHour, endHour);
        }
    }

    @Override
    public String toString() {
        return name;
    }
}
