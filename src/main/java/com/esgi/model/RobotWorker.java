package com.esgi.model;

import com.esgi.exception.EvilCorpException;
import com.esgi.utils.Log;

/**
 * Class of a robot worker
 */
public class RobotWorker extends Worker {

    /**
     * The constructor for a robot worker
     * @param name the name of the robot worker
     */
    public RobotWorker(String name) {
        super(name);
    }

    /**
     * Set a robot to standby for a specified duration
     * @param startHour the beginning time of the work
     * @param endHour the ending time of the work
     */
    public void standby(int startHour, int endHour) throws EvilCorpException {
        if(currentAffectation != null) {
            Log.register(currentAffectation, "Standby", startHour, endHour);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RobotWorker)) return false;

        RobotWorker that = (RobotWorker) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
