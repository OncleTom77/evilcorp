package com.esgi.model;

/**
 * Enum of the possible tasks where workers can be affected
 */
public enum Tasks {

    Task1, Task2, Task3
}
