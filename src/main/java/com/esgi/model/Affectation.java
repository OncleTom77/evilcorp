package com.esgi.model;

import com.esgi.utils.Utils;

import java.util.Date;

/**
 * Created by thomasfouan on 03/01/2017.
 */
public class Affectation {

    /**
     * The worker affected to the task
     */
    private Worker worker;

    /**
     * The beginning date of the task
     */
    private Date startTaskDate;

    /**
     * The task the worker is affected to
     */
    private Tasks task;

    /**
     * The constructor of an affectation
     * @param worker
     * @param startTaskDate
     * @param task
     */
    public Affectation(Worker worker, String startTaskDate, Tasks task) {
        this.worker = worker;
        this.startTaskDate = Utils.getDateFromString(startTaskDate);
        this.task = task;
    }

    public Worker getWorker() {
        return worker;
    }

    public Date getStartTaskDate() {
        return startTaskDate;
    }

    public Tasks getTask() {
        return task;
    }
}
