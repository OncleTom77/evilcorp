package com.esgi.exception;

/**
 * Class of computing exceptions of the application
 */
public class EvilCorpException extends Exception {

    /**
     * The code of the error
     */
    private String code;

    /**
     * The message of the error
     */
    private String message;

    /**
     * The cause of the error
     */
    private Throwable cause;

    /**
     * The constructor of the exception
     * @param code the code of the error
     * @param message the message of the error
     * @param cause the cause of the error
     */
    public EvilCorpException(String code, String message, Throwable cause) {
        this.code = code;
        this.message = message;
        this.cause = cause;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getCause() {
        return cause;
    }

    @Override
    public String toString() {
        return code + " : " + message;
    }
}
