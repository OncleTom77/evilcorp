package com.esgi.utils;

import com.esgi.exception.EvilCorpException;
import com.esgi.model.Affectation;

import java.io.*;

/**
 * Class managing the log of the activities
 */
public class Log {

    /**
     * Path to the file where the log are written to
     */
    private static final String PATH = "./file.csv";

    /**
     * Register an affectation of a worker in the log file
     * @param affectation
     * @param action
     * @param startHour
     * @param endHour
     */
    public static void register(Affectation affectation, String action, int startHour, int endHour) throws EvilCorpException {

        File file = new File(PATH);
        FileWriter fw = null;

        String data = "activity" + ","
                + affectation.getTask() + ","
                + affectation.getStartTaskDate().toString() + ","
                + affectation.getWorker().getName() + ","
                + action + ","
                + startHour + ","
                + endHour;

        try {
            fw = new FileWriter(file);

            fw.write(data + "\n");
        } catch (IOException e) {
            throw new EvilCorpException("01", "An error occurred when trying to register the affectation of a worker", e);
        } finally {
            if(fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
