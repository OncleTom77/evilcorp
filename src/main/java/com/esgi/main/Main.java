package com.esgi.main;

import com.esgi.exception.EvilCorpException;
import com.esgi.factory.Factory;
import com.esgi.model.HumanWorker;
import com.esgi.model.RobotWorker;
import com.esgi.model.Tasks;
import com.esgi.model.Worker;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by thomasfouan on 03/01/2017.
 */
public class Main {

    private static Factory factory;

    /**
     * Start an activity for a worker
     * @param worker the worker
     * @param activityName the name of the activity
     * @param begin the beginning hour of the activity
     * @param end the ending hour of the activity
     * @throws EvilCorpException
     */
    private static void doActivity(Worker worker, String activityName, String begin, String end) throws EvilCorpException {

        Method method;

        try {
            int startHour = Integer.parseInt(begin);
            int endHour = Integer.parseInt(end);

            method = worker.getClass().getDeclaredMethod(activityName, Integer.class, Integer.class);
            method.invoke(worker, startHour, endHour);
        } catch(NumberFormatException e) {
            throw new EvilCorpException("03", "Erreur lors de la saisie des heures", e);
        } catch (NoSuchMethodException e) {
            throw new EvilCorpException("04", "Erreur lors de la saisie de l'activité : " + activityName, e);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new EvilCorpException("05", "Erreur lors du lancement de l'activité '" + activityName + "' for the worker : '" + worker + "'", e);
        }
    }

    /**
     * Compute the registration of multiple workers
     * @param args the data to perform the action
     * @throws EvilCorpException
     */
    private static void computeMultipleInputs(String[] args) throws EvilCorpException {

        if(!args[1].equals("--file")) {
            throw new EvilCorpException("06", "Parametre inconnu pour '" + args[1] + "'", null);
        }

        File file = new File(args[2]);
        JSONParser jsonParser = new JSONParser();

        try {
            Object o = jsonParser.parse(new FileReader(file));
            JSONObject jsonObject = (JSONObject) o;

            JSONArray activities = (JSONArray) jsonObject.get("activities");
            JSONArray robots = (JSONArray) jsonObject.get("robots");
            JSONArray roles = (JSONArray) jsonObject.get("roles");

            for(int i=0; i< roles.size(); i++) {
                Object role = roles.get(i);
            }

            //TODO: FINISH THIS PART
        } catch (FileNotFoundException e) {
            throw new EvilCorpException("07", "Fichier '" + args[2] + "'introuvable", e);
        } catch (ParseException e) {
            throw new EvilCorpException("08", "Format du fichier incorrect", e);
        } catch (IOException e) {
            throw new EvilCorpException("06", "Erreur lors de la lecture du fichier '" + args[2] + "'", e);
        }
    }

    /**
     * Compute the registration of a single worker
     * @param args the data required to perform the action
     * @throws EvilCorpException
     */
    private static void computeSingleInput(String[] args) throws EvilCorpException {

        Worker worker = null;
        String date = null;

        if(args[1].equals("activity")) {
            worker = new HumanWorker(args[4]);
        } else if(args[1].equals("robot")) {
            worker = new RobotWorker(args[4]);
        }

        if(args.length == 8) {
            date = args[7];
        }

        factory.createAffectation(worker, date, Tasks.valueOf(args[2]));

        doActivity(worker, args[4], args[5], args[6]);
    }

    /**
     * Main function of the program
     * @param args
     */
    public static void main(String args[]) {

        factory = new Factory();

        try {
            switch (args.length) {
                case 3:
                    computeMultipleInputs(args);
                    break;
                case 7:
                case 8:
                    computeSingleInput(args);
            }
        } catch (EvilCorpException e) {
            System.out.println("ERROR : " + e);
        }
    }
}
