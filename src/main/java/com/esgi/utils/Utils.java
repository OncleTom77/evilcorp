package com.esgi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Utility Class of the project
 */
public class Utils {

    /**
     * Get a Date object from a String, like 2017-01-01
     * @param dateStr the String representing the date
     * @return an Object Date from the String representation of the date
     */
    public static Date getDateFromString(String dateStr) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date;

        try {
            date = sdf.parse(dateStr);
        } catch(ParseException e) {
            date = new Date();
        }

        return date;
    }
}
